// Databricks notebook source
// MAGIC %md #PhData Interview Presentation - Brian Bursaw#

// COMMAND ----------

// MAGIC %md ##Import Dataframes##

// COMMAND ----------

import org.apache.spark.sql.DataFrame

// COMMAND ----------

// MAGIC %md ##Add and Configure Widgets##

// COMMAND ----------

dbutils.widgets.text("S3_Location", "", "S3 Location")
dbutils.widgets.text("snowflake_warehouse", "", "Snowflake Warehouse")
dbutils.widgets.text("snowflake_database", "", "Snowflake Database")
dbutils.widgets.text("snowflake_schema", "", "Snowflake Schema")

// COMMAND ----------

// MAGIC %md ##Assign Widget Values to Variables##

// COMMAND ----------

val S3_LOCATION = dbutils.widgets.get ("S3_Location")
val SNOWFLAKE_WAREHOUSE = dbutils.widgets.get ("snowflake_warehouse")
val SNOWFLAKE_DATABASE = dbutils.widgets.get ("snowflake_database")
val SNOWFLAKE_SCHEMA = dbutils.widgets.get ("snowflake_schema")

// COMMAND ----------

// MAGIC %md ##Set Login Parameters##
// MAGIC Putting User and Password in the code due to free Databricks edition not allowing me to create a Secret Scope

// COMMAND ----------

// Use secrets DBUtil to get Snowflake credentials. - Putting User and Password in the code due to free Databricks edition not allowing me to create a Secret Scope
//val user = "jf71758"
val user = "BrianBursaw"
val password = "Temp12345@"

val options = Map(
  "sfUrl" -> "https://jf71758.west-us-2.azure.snowflakecomputing.com/",
  "sfUser" -> user,
  "sfPassword" -> password,
  "sfDatabase" -> "USER_BRIAN",
  "sfSchema" -> "PUBLIC",
  "sfWarehouse" -> "INTERVIEW_WH"
)

// COMMAND ----------

// MAGIC %md ##Read Data From Snowflake##

// COMMAND ----------

// Read the data written by the previous cell back.
val airlinesdf = spark.read
  .format("snowflake")
  .options(options)
  .option("dbtable", "USER_BRIAN.PUBLIC.AIRLINES")
  .load()
val airportsdf = spark.read
  .format("snowflake")
  .options(options)
  .option("dbtable", "USER_BRIAN.PUBLIC.AIRPORTS")
  .load()
val flightsdf = spark.read
  .format("snowflake")
  .options(options)
  .option("dbtable", "USER_BRIAN.PUBLIC.FLIGHTS")
  .load()



// COMMAND ----------

// MAGIC %md ##Write Data to DataFrames##

// COMMAND ----------

import org.apache.spark.sql.SaveMode

val user = "BrianBursaw"
val password = "Badger169!"

val options = Map(
  "sfUrl" -> "https://jf71758.west-us-2.azure.snowflakecomputing.com/",
  "sfUser" -> user,
  "sfPassword" -> password,
  "sfDatabase" -> "USER_BRIAN",
  "sfSchema" -> "PUBLIC",
  "sfWarehouse" -> "INTERVIEW_WH"
)

airlinesdf.write
.format("snowflake")
.options(options)
.option("dbtable", "USER_BRIAN.PUBLIC.AIRLINES")
//.mode(SaveMode.Overwrite)
//.save()

airportsdf.write
.format("snowflake")
.options(options)
.option("dbtable", "USER_BRIAN.PUBLIC.AIRPORTS")
//.mode(SaveMode.Overwrite)
//.save()

flightsdf.write
.format("snowflake")
.options(options)
.option("dbtable", "USER_BRIAN.PUBLIC.FLIGHTS")
//.mode(SaveMode.Overwrite)
//.save()

// COMMAND ----------

// MAGIC %md ##Create a Temp View from DataFrames##

// COMMAND ----------

airlinesdf.createOrReplaceTempView("airlines")
airportsdf.createOrReplaceTempView("airports")
flightsdf.createOrReplaceTempView("flights")


// COMMAND ----------

// MAGIC %md ##List Flights Columns##

// COMMAND ----------

// MAGIC %sql
// MAGIC 
// MAGIC DESC flights

// COMMAND ----------

// MAGIC %md ##REPORT 1 - Total number of flights by airline and airport on a monthly basis##

// COMMAND ----------

// MAGIC %sql
// MAGIC 
// MAGIC SELECT a.AIRLINE, count (*) AS NUMBER_OF_FLIGHTS
// MAGIC FROM FLIGHTS AS f
// MAGIC JOIN AIRLINES AS a ON f.AIRLINE = a.IATA_CODE
// MAGIC WHERE MONTH = 5
// MAGIC GROUP by a.AIRLINE
// MAGIC ORDER by NUMBER_OF_FLIGHTS

// COMMAND ----------

// MAGIC %md ##REPORT - Cancellation percentage by airline##

// COMMAND ----------

// MAGIC %sql
// MAGIC 
// MAGIC SELECT a.AIRLINE, SUM(CANCELLED) AS CANCELLED, count (*) AS TOTAL_FLIGHTS, (SUM(CANCELLED)/ count(*)) * 100 AS CANCELLATION_PERCENTAGE
// MAGIC FROM FLIGHTS AS f
// MAGIC JOIN AIRLINES AS a ON f.AIRLINE = a.IATA_CODE
// MAGIC GROUP by a.AIRLINE
// MAGIC ORDER by TOTAL_FLIGHTS, CANCELLED, CANCELLATION_PERCENTAGE

// COMMAND ----------

// MAGIC %md ##REPORT 2 -  On time percentage of each airline for the year 2015##

// COMMAND ----------

// MAGIC %sql
// MAGIC 
// MAGIC SELECT a.AIRLINE, count (*) AS TOTAL_FLIGHTS, count(AIR_SYSTEM_DELAY + SECURITY_DELAY + AIRLINE_DELAY + LATE_AIRCRAFT_DELAY + WEATHER_DELAY) AS DELAYED, 100 - (count(AIR_SYSTEM_DELAY + SECURITY_DELAY + AIRLINE_DELAY + LATE_AIRCRAFT_DELAY + WEATHER_DELAY)/ count(*)) * 100 AS ON_TIME_PERCENTAGE
// MAGIC FROM FLIGHTS AS f
// MAGIC JOIN AIRLINES AS a ON f.AIRLINE = a.IATA_CODE
// MAGIC WHERE YEAR = "2015"
// MAGIC GROUP by a.AIRLINE
// MAGIC ORDER by TOTAL_FLIGHTS, DELAYED

// COMMAND ----------

// MAGIC %md ##REPORT 3 -  Airlines with the largest number of delays##

// COMMAND ----------

// MAGIC %sql
// MAGIC 
// MAGIC SELECT a.AIRLINE, count(AIR_SYSTEM_DELAY + SECURITY_DELAY + AIRLINE_DELAY + LATE_AIRCRAFT_DELAY + WEATHER_DELAY) AS DELAYED
// MAGIC FROM FLIGHTS AS f
// MAGIC JOIN AIRLINES AS a ON f.AIRLINE = a.IATA_CODE
// MAGIC GROUP by a.AIRLINE
// MAGIC ORDER by DELAYED

// COMMAND ----------

// MAGIC %md ##REPORT 4 -  Cancellation reasons by airport##

// COMMAND ----------

// MAGIC %sql
// MAGIC 
// MAGIC SELECT f.ORIGIN_AIRPORT, count(CASE WHEN CANCELLATION_REASON = 'A' THEN 'Airline/Carrier' END) AS AIRLINE_CARRIER,
// MAGIC count(CASE WHEN CANCELLATION_REASON = 'B' THEN 'Weather Delay' END) AS WEATHER,
// MAGIC count(CASE WHEN CANCELLATION_REASON = 'C' THEN 'National Air System' END) AS NATIONAL_AIR_SYSTEM,
// MAGIC count(CASE WHEN CANCELLATION_REASON = 'D' THEN 'SECURITY' END) AS SECURITY
// MAGIC FROM FLIGHTS AS f
// MAGIC WHERE f.ORIGIN_AIRPORT = 'DEN'
// MAGIC GROUP by f.ORIGIN_AIRPORT
// MAGIC ORDER by f.ORIGIN_AIRPORT, AIRLINE_CARRIER, WEATHER

// COMMAND ----------

// MAGIC %md
// MAGIC ##Bar graph works well when sorting by airline##

// COMMAND ----------

// MAGIC %sql
// MAGIC 
// MAGIC SELECT a.AIRLINE, count(CASE WHEN CANCELLATION_REASON = 'A' THEN 'Airline/Carrier' END) AS AIRLINE_CARRIER,
// MAGIC count(CASE WHEN CANCELLATION_REASON = 'B' THEN 'Weather Delay' END) AS WEATHER,
// MAGIC count(CASE WHEN CANCELLATION_REASON = 'C' THEN 'National Air System' END) AS NATIONAL_AIR_SYSTEM,
// MAGIC count(CASE WHEN CANCELLATION_REASON = 'D' THEN 'SECURITY' END) AS SECURITY
// MAGIC FROM FLIGHTS AS f
// MAGIC JOIN AIRLINES AS a ON f.AIRLINE = a.IATA_CODE
// MAGIC GROUP by a.AIRLINE
// MAGIC ORDER by AIRLINE

// COMMAND ----------

// MAGIC %md ##REPORT 5 -  Delay reasons by airport##

// COMMAND ----------

// MAGIC %sql
// MAGIC 
// MAGIC SELECT f.ORIGIN_AIRPORT, count(CASE WHEN AIRLINE_DELAY > 0 THEN 'Airline Delay' END) AS AIRLINE_DELAY,
// MAGIC count(CASE WHEN AIR_SYSTEM_DELAY > 0 THEN 'Air System Delay' END) AS AIR_SYSTEM_DELAY,
// MAGIC count(CASE WHEN SECURITY_DELAY > 0 THEN 'Security Delay'END) AS SECURITY_DELAY,
// MAGIC count(CASE WHEN LATE_AIRCRAFT_DELAY > 0 THEN 'Late Aircraft Delay' END) AS LATE_AIRCRAFT_DELAY,
// MAGIC count(CASE WHEN WEATHER_DELAY > 0 THEN 'Weather Delay' END) AS WEATHER_DELAY
// MAGIC FROM FLIGHTS AS f
// MAGIC WHERE f.ORIGIN_AIRPORT = 'DEN'
// MAGIC GROUP by f.ORIGIN_AIRPORT
// MAGIC ORDER by f.ORIGIN_AIRPORT

// COMMAND ----------

// MAGIC %md ##REPORT 6 -  Airline with the most unique routes##

// COMMAND ----------

// MAGIC %sql
// MAGIC 
// MAGIC SELECT a.AIRLINE, COUNT(DISTINCT FLIGHTS.AIRLINE, ORIGIN_AIRPORT, DESTINATION_AIRPORT) AS UNIQUE_ROUTES
// MAGIC FROM FLIGHTS
// MAGIC JOIN AIRLINES AS a ON FLIGHTS.AIRLINE = a.IATA_CODE
// MAGIC GROUP by a.AIRLINE
